#!/bin/bash

IP=192.168.0.66 # hardcoded
RT=192.168.0.86
EN=$(ip -4 route ls |awk '/^default/{print $5}')

case $1 in
setup)
  cat << EOF > /etc/network/interfaces.d/eth0
 auto eth0:0
 allow-hotplug eth0:0
 iface eth0:0 inet static
    address 192.168.0.66
    netmask 255.255.255.0
EOF
  apt install tftpd-hpa
  chmod o+rw /srv/tftp
;;
firmware) # http://www.tp-link.com/us/download/TL-WR842ND.html
  # curl -LOJ http://www.tp-link.com/resources/software/TL-WR842ND_V2_130628.zip
  curl -LOJ https://static.tp-link.com/res/down/soft/TL-WR842ND_V2_150514.zip
;;
recovery)
  ln -sf *-squashfs-factory.bin 'wr842nv2_tp_recovery.bin'
;;
ip)
  sudo ip a add $IP/24 dev $EN
;;
tcpdump)
  sudo tcpdump -n -i $EN host $RT
;;
tftpd)
  [ -e ./tftpd/tftpd ] || (cd tftpd && go build)
  sudo ./tftpd/tftpd
;;
sync)
  rsync -avP --no-times --no-owner --no-group *.bin $HOST:/srv/tftp/
;;
esac
