# TL-WR842ND-V2
CONFIG_TARGET_ath79=y
CONFIG_TARGET_ath79_generic=y
CONFIG_TARGET_ath79_generic_DEVICE_tplink_tl-wr842n-v2=y
CONFIG_DEVEL=y
CONFIG_CCACHE=n

# reduce size (https://openwrt.org/docs/guide-user/additional-software/saving_space)
CONFIG_KERNEL_KALLSYMS=n
CONFIG_KERNEL_DEBUG_KERNEL=n
CONFIG_KERNEL_DEBUG_INFO=n
CONFIG_KERNEL_PRINTK=y
CONFIG_KERNEL_CRASHLOG=n
CONFIG_KERNEL_ELF_CORE=n
CONFIG_KERNEL_MAGIC_SYSRQ=n
CONFIG_KERNEL_PRINTK_TIME=y
CONFIG_PACKAGE_MAC80211_DEBUGFS=n
CONFIG_PACKAGE_MAC80211_MESH=n
CONFIG_STRIP_KERNEL_EXPORTS=y
CONFIG_USE_MKLIBS=y
CONFIG_EARLY_PRINTK=n
CONFIG_SERIAL_8250=n
CONFIG_PACKAGE_opkg=n
CONFIG_PACKAGE_ppp=n
CONFIG_PACKAGE_kmod-ppp=n
CONFIG_PACKAGE_kmod-pppoe=n
CONFIG_PACKAGE_kmod-pppox=n

# reduce ram
CONFIG_PACKAGE_zram-swap=y
CONFIG_PROCD_ZRAM_TMPFS=y

# busybox
CONFIG_BUSYBOX_CUSTOM=n
CONFIG_BUSYBOX_CONFIG_WATCH=n

# ipv6
CONFIG_IPV6=y
CONFIG_PACKAGE_ipv6-support=y
CONFIG_PACKAGE_kmod-ipv6=y
CONFIG_PACKAGE_6in4=y
CONFIG_PACKAGE_miredo=n
CONFIG_PACKAGE_kmod-ipt-nat6=n

# vlan
CONFIG_PACKAGE_kmod-switch-ip17xx=y
CONFIG_PACKAGE_kmod-switch-rtl8366-smi=y
CONFIG_PACKAGE_kmod-switch-rtl8366s=y

# wifi
CONFIG_PACKAGE_wpad-basic-wolfssl=n
CONFIG_PACKAGE_wpad-basic-mbedtls=n

# core services
CONFIG_PACKAGE_miniupnpd-nftables=y
CONFIG_PACKAGE_sqm-scripts=n
CONFIG_PACKAGE_nft-qos=n
CONFIG_PACKAGE_pbr=n

# admin services
CONFIG_PACKAGE_muninlite=n

# p910nd service
CONFIG_PACKAGE_kmod-usb-printer=y
CONFIG_PACKAGE_avahi-nodbus-daemon=y
CONFIG_PACKAGE_p910nd=y

# bittorent opentracker
CONFIG_PACKAGE_libowfat=n
CONFIG_PACKAGE_opentracker=n

# samba
CONFIG_PACKAGE_samba36-server=n

# torrent servers/clients
CONFIG_PACKAGE_transmission-daemon=n
CONFIG_PACKAGE_transmission-cli=n
CONFIG_PACKAGE_transmission-remote=n

# adblock
CONFIG_PACKAGE_adblock=n
CONFIG_PACKAGE_uhttpd=n

# pptpd
CONFIG_PACKAGE_kmod-gre=n
CONFIG_PACKAGE_pptpd=n
CONFIG_PACKAGE_kmod-mppe=n

# wireguard
CONFIG_PACKAGE_kmod-wireguard=y
CONFIG_PACKAGE_wireguard=y
CONFIG_PACKAGE_wireguard-tools=y

# iptv
CONFIG_PACKAGE_igmpproxy=n

# network utils
CONFIG_PACKAGE_coreutils=y
CONFIG_PACKAGE_coreutils-nohup=n
CONFIG_PACKAGE_tcpdump=y
CONFIG_PACKAGE_rsync=y
CONFIG_PACKAGE_bmon=n
CONFIG_PACKAGE_iperf3=n
CONFIG_PACKAGE_iftop=n
CONFIG_PACKAGE_lsof=n
CONFIG_PACKAGE_curl=n
CONFIG_PACKAGE_telnet-bsd=n
CONFIG_PACKAGE_socat=n
CONFIG_SOCAT_SSL=n

# utils
CONFIG_PACKAGE_sexpect=n

# storage usb
CONFIG_PACKAGE_tune2fs=n
CONFIG_PACKAGE_hdparm=n
CONFIG_PACKAGE_e2fsprogs=n
CONFIG_PACKAGE_kmod-usb-storage-extras=n
CONFIG_PACKAGE_kmod-usb-uhci=n
CONFIG_PACKAGE_block-mount=n
CONFIG_PACKAGE_kmod-fs-ext4=n
CONFIG_PACKAGE_kmod-fs-msdos=n
CONFIG_PACKAGE_kmod-fs-vfat=n
CONFIG_PACKAGE_kmod-fs-ntfs=n
CONFIG_PACKAGE_kmod-fs-btrfs=n
CONFIG_PACKAGE_kmod-usb-storage=n
CONFIG_PACKAGE_fdisk=n
CONFIG_PACKAGE_blkid=n
CONFIG_PACKAGE_usb-modeswitch=n
CONFIG_PACKAGE_usbutils=n

# pptp client
CONFIG_PACKAGE_ppp-mod-pptp=n

# openvpn client
CONFIG_PACKAGE_kmod-tun=n
CONFIG_PACKAGE_openvpn-wolfssl=n

# adb
CONFIG_PACKAGE_adb=n
