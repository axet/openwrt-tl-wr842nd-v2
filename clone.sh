#!/bin/bash

set -e

which apt-get && sudo apt-get install -y libncurses5-dev gawk subversion zlib1g-dev libssl-dev libxml-parser-perl rsync

git clone -b v23.05.5 https://git.openwrt.org/openwrt/openwrt.git trunk
